<%@ page contentType = "text/html; charset = UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
   <head>
      <title>Lista użytkowników z możliwością filtrowania</title>
   </head>

   <body>
   <form method="get" action="searchUsers">
      <div>
        <input type="text" id ="txt" name="name" >
        <button id="button-id" type="submit">Wyszukaj użytkowników</button>
      </div>
      <table border="1">
          <tr>
              <th>Imię</th>
              <th>Wiek</th>
          </tr>
          <c:forEach items="${users}" var="user" >
              <tr>
                  <td>${user.name}</td>
                  <td>${user.age}</td>
                  <td>
                      <!-- zawsze gdy adres jest wpisany href to mamy metode get -->
                    <a href="user/${user.id}/update"> Aktualizuj użytkownika</a>
                  </td>
                  <td>
                  <a href="user/${user.id}/delete"> Usuń użytkownika</a>
                  </td>
              </tr>
          </c:forEach>
      </table>
      </form>
   </body>
</html>