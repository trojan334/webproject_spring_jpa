package pl.sda.ldz13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.model.User;
import pl.sda.ldz13.repository.UserJpaRepository;

import java.util.List;

@Service
public class UserServiceSingleton implements UserService {

    @Autowired
    private UserJpaRepository userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User getUserById(Long id) {

        return userRepository.findById(id).get();
    }

    @Override
    public User modifyUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public boolean deleteUser(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception e){
            return false;
        }
        return true;
    }
    @Override
    public List<User> getUsersByName(String name) {
        return userRepository.findByNameIgnoreCaseContaining(name);
    }


    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getUsersByNameAndAge(String name, int age) {
        return userRepository.findNameAndAge(name,age);
    }
}
