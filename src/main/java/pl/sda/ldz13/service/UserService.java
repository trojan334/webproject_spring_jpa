package pl.sda.ldz13.service;

import pl.sda.ldz13.model.User;

import java.util.List;

public interface UserService {
    User addUser(User user);

    User getUserById(Long id);

    User modifyUser(User user);

    boolean deleteUser(Long id);

    List<User> getUsersByName(String name);

    List<User> getAllUsers();

    List<User> getUsersByNameAndAge (String name, int age);
}
